# frozen_string_literal: true

require_relative "jestery/version"

module Jestery
  class Error < StandardError; end
  # Your code goes here...
end
